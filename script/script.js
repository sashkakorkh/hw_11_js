const form = document.querySelector('.password-form');
const inputPassword = document.querySelector('.password');
const inputSubmitPassword = document.querySelector('.submit-password')
const btn = document.querySelector('.btn')
const inputAlert = document.querySelector('.inputAlert')

function enterPassword() {
    form.addEventListener('click', e => {
        let target = e.target;
        if (target.tagName === 'BUTTON') {
            confirmPassword(e);
        }
        if (target.tagName === 'I') {
            changeVisibility(target);
        }

        if (inputPassword === document.activeElement || inputSubmitPassword === document.activeElement) {
            inputAlert.classList.add('visibility')
        }
    })
}


function confirmPassword(event) {
    event.preventDefault();
        const valuePassword = inputPassword.value;
        const valueSubmit = inputSubmitPassword.value;
        if (!valuePassword || (!/\S/.test(valuePassword) && !valueSubmit || (!/\S/.test(valueSubmit)))) {
            confirm('Enter password');
        } else {
            valuePassword === valueSubmit ? alert('You are welcome') : inputAlert.classList.remove('visibility');
        }
}

function changeVisibility(target) {
    if (!(target.matches('.icon-password'))) {
        return;
    } else {
        target.classList.toggle('fa-eye-slash');
        let input = target.previousElementSibling
        let inputType = input.getAttribute('type') === 'password' ? 'type' : 'password';
        input.setAttribute('type', inputType)
    }
}

enterPassword()